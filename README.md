# Designer Interview Questions

This is a collection of interview questions to ask a new candidate, or to ask yourself. I plan on fleshing them out some more, and adding the answers I generally look for from a candidate.

See something wrong? Have a suggestion? [Submit pull request.](https://github.com/mrbrianhinton/designer-interview-questions/pulls)

## Interview Questions

## Phone Questions

1. Describe your journey towards deciding on a career as a Designer.
    - Objective: Open-ended question that can lead to discovery, and insight about the candidates goals.
    
2. Describe to me what you did to prep for this interview?
    - Objective: Determining how well they prepare for the interview can identify the research capabilities they have for projects, and even demonstrates their interest in the role they are applying.
    
3. Describe how you arrived to this interview?
    - Objective: Visual analysis. How do they think? Logically? Or visually?
    
4. What tools do you use?
    - Objective: Open-ended question. Are they opinionated about tools? Or open to the right tool for the job.
    
5. Explain the concept of UX design to a 5 year old?
    - Objective: Can they position themselves into a persona? 
    
6. In a job what would you do with any spare time you might have?
    - Objective: Personal growth? Is it important to the candidate that they pursue activities that grow their skillset, and expand their capabilities?
    
7. Describe your process and approach to Design?
    - Objective: See if they have an existing process, and how they work. Do they follow a process, or just jump right in? Can they follow a process? Our looping process: Research, Document, Pencil Sketch (ideation), Quantify Design (wireframes and basic prototypes), Approval, Document, Final Designs, Approval, Document, Implementation (code), Review Implementation, Approval, Release, Rinse and Repeat)
    
8. What if a design of yours wasn’t implemented correctly by a Front-end Developer, what would be your response?
    - Objective: No right answer. Checking for logical thinking, and openness to work with developers to understand the change.
    
9. What do you know about the company and services/products we sell?
    - Objective: Follow up on question 2. to evaluate their knowledge.
    
10. When designing an interface flow how do you know that what you are designing is tailored for the user?
    - Objective: Checking for basic understanding of user research
    
11. How do you know when a design is done?
    - Objective: Many would say when you can't remove anything else. Simplified to the bare minimum. My answer is that design is continously iterative, but a design is complete when the projects requirements have been met. But it is never truly done.
    
12. What would you do when a business sponsor rejects a design?
    - Objective: A business sponsor is the "stakeholder" for a project. They are the final judge on if requirements and project objectives are being met.
    
13. Can you describe a time when the requirements of a project changed in the middle of it's design, and how you handled that situation?
    - Objective: While impossible to see how they would truly act you will surprisingly get some insight into the person. Some people I've asked this question have expressed genuine anger at this occurring.
    
14. Describe designing within an agile process.
    - Objective: Do they understand the process we are using? What education will they need to get up to speed.
    
15. If you design something and a developer told you “we can’t do that,” what would you do?
    - Objective: How would they work with engineering to solve problems.
    
16. How would former co-workers describe you as someone to work with? What negative, and what positive things would they say about you?
    - Objective: Can they communicate both negative and positive problems they may have? Are they aware of areas of improvement within themselves?
    
17. What are the biggest tech innovations or trends right now that you think we should apply to our industry?
    - Objective: Are they aware of trends, and technology?
    
18. Do you prefer to work alone or with a team?
    - Objective: An honest answer will better see how, and where the candidate could fit within a team. Solo workers aren't necessarily bad, but can work independently.
    
19. How would you like to grow as a designer? Are their topics that you see need for improvement?
    - Objective: Everyone can grow. Do they recognize their own weaknesses?

### In-person Interview

20. Let's whiteboard the design of a TV built for a visually impaired or blind individual?
    - Objective: Visual thinking on an uncommon problem that is purposefully distant from simply drawing a wireframe.
    
21. Think about the journey you took to arrive for the interview today. Now on the whiteboard, while talking to me, visually communicate the journey. How you want to visualize this is up to you.
    - Objective: Can they visualize thoughts on the whiteboard? Do words flow while communicating visually?
